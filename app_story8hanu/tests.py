from django.test import TestCase, Client
from django.urls import resolve
from .views import home
from selenium import webdriver
from selenium.common.exceptions import StaleElementReferenceException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import time

class Story8UnitTest(TestCase):
    def test_story8_url_exists(self):
        url_response = Client().get('/')
        self.assertEqual(url_response.status_code, 200)

    def test_story8_use_template(self):
        url_response = Client().get('/')
        self.assertTemplateUsed(url_response, "home.html")

class Story8FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_input_message(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        
        time.sleep(10)
        
        up = selenium.find_element_by_css_selector('a.up')
        down = selenium.find_element_by_css_selector('a.down')
        up.click()
        down.click()
